/*
 * Vytvořeno aplikací SharpDevelop.
 * Uživatel: Adam
 * Datum: 16. 12. 2013
 * Čas: 14:43
 * 
 * Tento template můžete změnit pomocí Nástroje | Možnosti | Psaní kódu | Upravit standardní hlavičky souborů.
 */
using System;

namespace game
{
    class Program
    {
        public static void Main(string[] args)
        {
    
            string[,] pole = new string[25,25];
            
            for (int y = 24; y > 1; y--) {        //okraje mapy
                for (int x = 1; x < 25; x++) {
                    
                
                    pole[x,y]=" ";
                    if (x==1 ||y==2 || y==24 || x==24) {
                        pole[x,y]="#";
                    }
            
                    
                }
                Console.WriteLine();
            }
            
            int horizont = 2; // výchozí podmínky
            int vertikal = 3;
            int klic = 0;
            pole[15,14]="?";
            pole[5,6]="?";
            pole[19,3]="?";
            
            do {
                Console.WriteLine("Hru ovládáte zadáváním WASD - bude se pohybovat postava reprezentovana symbolem O. Sesbírejte 3 klíče k úniku z kyberprostoru");
                Console.WriteLine("(Poznámka k ovládání: zadejte např. w a pak enter)");
                Console.WriteLine();
                pole[horizont,vertikal]="O";         //vykreslování mapy
                for (int y = 24; y > 1; y--) {
                for (int x = 1; x < 25; x++) {
                    
                        if (pole[x,y]=="O" || pole[x,y]=="?") {
                    Console.Write(" {0}",pole[x,y]);
                    continue;
                }
                    pole[x,y]=" ";
                    if (x==1 ||y==2 || y==24 || x==24) {
                        pole[x,y]="#";
                    }
                    Console.Write(" {0}",pole[x,y]);
                    
                }
                Console.WriteLine();
            }
                Console.WriteLine();
                Console.WriteLine("Počet klíčů: {0}",klic);
                pole[horizont,vertikal]=" ";
                string smer = Console.ReadLine();        // ovládání a kolize
                if (smer=="w") {
                    vertikal++;
                    if (pole[horizont,vertikal]=="#") {
                        vertikal--;
                    }
                    if (pole[horizont,vertikal]=="?") {
                        klic++;
                    }
                }
                
                if (smer=="s") {
                vertikal--;    
                if (pole[horizont,vertikal]=="#") {
                        vertikal++;
                    }
                if (pole[horizont,vertikal]=="?") {
                        klic++;
                    }
                
                }
                
                if (smer=="a") {
                horizont--;
                if (pole[horizont,vertikal]=="#") {
                        horizont++;
                    }
                if (pole[horizont,vertikal]=="?") {
                        klic++;
                    }
                }
                
                if (smer=="d") {
                horizont++;
                if (pole[horizont,vertikal]=="#") {
                        horizont--;
                    }
                if (pole[horizont,vertikal]=="?") {
                        klic++;
                    }
                }
                
                Console.Clear();
                
            } while (klic<3);
            
            Console.WriteLine("Unikli jste z kybernetického vězení - blahopřeji! Tvůrce: Adam Filandr");
            Console.Write("Press any key to continue . . . ");
            Console.ReadKey(true);
        }
    }
}